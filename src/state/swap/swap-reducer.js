import { useEffect } from "react";

let ACTIONS = {
  SET_FIRST_CURRENCY: "SET_FIRST_CURRENCY",
  SET_SECOND_CURRENCY: "SET_SECOND_CURRENCY",
};

export let initialState = {
  initialized: false,
  firstCurrency: "",
  secondCurrency: "",
};

const swapReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.SET_FIRST_CURRENCY:
      return {
        ...state,
        initialized: true,
        firstCurrency: action.currency,
      };
    case ACTIONS.SET_SECOND_CURRENCY:
      return {
        ...state,
        initialized: true,
        secondCurrency: action.currency,
      };

    default:
      return state;
  }
};

//Actions

export function setFirstCurrency(currency) {
  return {
    type: ACTIONS.SET_FIRST_CURRENCY,
    currency,
  };
}
export function setSecondCurrency(currency) {
  return {
    type: ACTIONS.SET_SECOND_CURRENCY,
    currency,
  };
}

export default swapReducer;
