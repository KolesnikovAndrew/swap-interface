import { applyMiddleware, createStore, compose, combineReducers } from "redux";

import thunkMiddleware from "redux-thunk";
import swapReducer from "./swap/swap-reducer";

let reducers = combineReducers({
  swap: swapReducer,
});

const composeEnhansers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore(
  reducers,
  composeEnhansers(applyMiddleware(thunkMiddleware))
);

window.store = store;

export default store;
