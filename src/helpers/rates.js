export let ratesData = [
  { base: "ETH", rates: { ETH: 1, USDC: 2, WBTC: 0.5, WETH: 35 } },
  { base: "USDC", rates: { ETH: 0.5, WBTC: 0.25, WETH: 17.5, USDC: 1 } },
  { base: "WBTC", rates: { ETH: 2, WBTC: 1, WETH: 70, USDC: 4 } },
  { base: "WETH", rates: { ETH: 0.028, WBTC: 0.014285, WETH: 1, USDC: 0.057 } },
];

export default ratesData;
