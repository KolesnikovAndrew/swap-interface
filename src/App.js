import { connect } from "react-redux";
import React from "react";
import { Provider } from "react-redux";
import { compose } from "redux";
import "./App.css";
import SwapContainer from "./components/swap/Swap";
import store from "./state/store";

function App() {
  return (
    <div className="App">
      <SwapContainer />
    </div>
  );
}

const mapStateToProps = (state) => ({
  initialized: state.swap.initialized,
  firstCurrency: state.swap.firstCurrency,
});

let AppContainer = compose(connect(mapStateToProps, null))(App);

const MainApp = (props) => {
  return (
    <Provider store={store}>
      <AppContainer />
    </Provider>
  );
};

export default App;
