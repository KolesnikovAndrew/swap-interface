import React, { useEffect, useState, useReducer } from "react";
import { Input } from "../input/Input";
import ratesData from "../../helpers/rates";
import swapReducer, { initialState } from "../../state/swap/swap-reducer";
import SwapArrow from "./SwapArrow";
export const Swap = (props) => {
  let [firstCurr, setFirstCurr] = useState("ETH");
  let [secondCurr, setSecondCurr] = useState("ETH");
  let [amount, setAmount] = useState(1);
  let [exchangeRate, setExchangeRate] = useState();
  const [swap, dispatch] = useReducer(swapReducer, initialState);
  const [amountInFromCurrency, setAmountInFromCurrency] = useState(true);

  let toAmount, fromAmount;
  if (amountInFromCurrency) {
    fromAmount = amount;
    toAmount = amount * exchangeRate || 0;
  } else {
    toAmount = amount;
    fromAmount = amount / exchangeRate;
  }

  useEffect(() => {
    if (firstCurr != null && secondCurr != null) {
      for (let i = 0; i < ratesData.length; i++) {
        if (ratesData[i].base == firstCurr) {
          setExchangeRate(ratesData[i].rates[secondCurr]);
        }
      }
    }
  }, [firstCurr, secondCurr]);

  //Set amount of currency converted
  function handleFromAmountChange(e) {
    setAmount(e.target.value);
    setAmountInFromCurrency(true);
  }
  function handleToAmountChange(e) {
    setAmount(e.target.value);
    setAmountInFromCurrency(false);
  }

  return (
    <div>
      <div>
        <h1>Swap</h1>
      </div>
      <div id="first-currency-selector">
        <Input
          amount={fromAmount}
          setFirstCurr={setFirstCurr}
          selectedCurrency={firstCurr}
          onChangeAmount={handleFromAmountChange}
          id="first"
        />
      </div>

      <SwapArrow
        setFirstCurr={setFirstCurr}
        setSecondCurr={setSecondCurr}
        firstCurr={firstCurr}
        secondCurr={secondCurr}
        setAmountInFromCurrency={setAmountInFromCurrency}
        amountInFromCurrency={amountInFromCurrency}
      />

      <div id="second-currency-selector">
        <Input
          amount={toAmount}
          setSecondCurr={setSecondCurr}
          selectedCurrency={secondCurr}
          onChangeAmount={handleToAmountChange}
          id="second"
        />
      </div>
    </div>
  );
};

export default Swap;
