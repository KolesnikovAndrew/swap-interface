import React from "react";

export const SwapArrow = (props) => {
  let swapCurrencies = () => {
    props.setFirstCurr(props.secondCurr);
    props.setSecondCurr(props.firstCurr);
    props.setAmountInFromCurrency(!props.amountInFromCurrency);
  };

  return (
    <div>
      <button onClick={swapCurrencies}>Swap Currencies</button>
    </div>
  );
};

export default SwapArrow;
