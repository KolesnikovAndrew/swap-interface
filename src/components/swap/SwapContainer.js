import React from "react";
import { connect } from "react-redux";
import { compose } from "redux";

import {
  setFirstCurrency,
  setSecondCurrency,
} from "../../state/swap/swap-reducer";
import Swap from "./Swap";

class SwapContainer extends React.Component {
  render() {
    return (
      <div>
        <Swap setFirstCurrency={setFirstCurrency} />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    firstCurrency: state.swap.firstCurrency,
    secondCurrency: state.swap.secondCurrency,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setFirstCurrency: (newCurrency) => dispatch(setFirstCurrency(newCurrency)),
  };
};

const SwapContainer = connect(mapStateToProps, mapDispatchToProps)(Swap);

export default SwapContainer;
