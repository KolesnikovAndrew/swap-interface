import React, { useEffect, useState } from "react";

export const Input = (props) => {
  const onCurrencyChange = (e) => {
    if (props.id == "first") {
      return props.setFirstCurr(e.target.value);
    }
    if (props.id == "second") {
      return props.setSecondCurr(e.target.value);
    }
  };

  return (
    <div>
      <input
        placeholder="0.0"
        type="number"
        value={Math.round(props.amount * 100) / 100}
        onChange={props.onChangeAmount}
      />
      <select
        onChange={onCurrencyChange}
        name="currencies-select"
        id={props.id}
        value={props.selectedCurrency}
      >
        <option value="ETH">ETH</option>
        <option value="USDC">USDC</option>
        <option value="WBTC">WBTC</option>
        <option value="WETH">WETH</option>
      </select>
    </div>
  );
};

export default Input;
